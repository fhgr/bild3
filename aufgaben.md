# Aufgaben Bildverarbeitung 3

## MNIST Datensatz

Wir arbeiten in dieser Übung mit dem MNIST-Datensatz. Dieser besteht aus 70'000 Graustufen-Bildern von handgeschriebenen Ziffern von 0 bis 9, welche jeweils 28x28 Pixel gross sind. 60'000 Bilder sind bestimmt als Trainingsdaten, 10'000 Bilder als Testdaten. Ziel ist es, für jedes Element der Testdaten die im Bild enthaltene Ziffer zu bestimmen. Es handelt sich daher um eine Klassifikationsaufgabe. 

## Setup

Verwenden Sie eine Ubuntu Distribution, z.B. 18.04.

Installieren Sie einige benötigte Programme:

```bash
sudo apt update

sudo apt install -y python3-pip git vim

pip3 install pygame jupyter tensorflow keras scikit-learn scikit-image sklearn matplotlib pandas seaborn # ohne sudo!
```

Loggen Sie sich anschliessend neu ein oder starten Sie Ubuntu neu.

Wir arbeiten mit einem Jupyter Notebook. Laden Sie dieses mittels folgendem Befehl herunter:

```bash
git clone https://gitlab.com/fhgr/bild3.git
```

Starten Sie Jupyter wie folgt:

```bash
cd bild3
jupyter notebook
```

Es sollte sich ein Browserfenster öffnen, wählen Sie in diesem die Datei `mnist.ipynp` und folgen Sie dem Inhalt dieses Notebooks.



